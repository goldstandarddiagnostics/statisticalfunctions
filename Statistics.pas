﻿namespace Essy.Math;

interface

uses
  System.Linq,
  System.Collections.Generic;

type
  Statistical = public static class
  private    
  protected
  public
    /// <summary>
    /// Calculates the Standard Deviation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    class method StandardDeviation_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the Standard Deviation on the sample population (suggested method)
    /// </summary>
    /// <param name="valueList"></param>
    class method StandardDeviation_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the Coefficient of variation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    class method CV_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the Coefficient of variation on the sample population (suggested method)
    /// </summary>
    /// <param name="valueList"></param>
    class method CV_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the Coefficient of variation on the whole population, percentage goes from 0 to 100
    /// </summary>
    /// <param name="valueList"></param>
    class method CVPercentage_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the Coefficient of variation on the sample population, percentage goes from 0 to 100 (suggested method)
    /// </summary>
    /// <param name="valueList"></param>
    class method CVPercentage_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average of a number of values
    /// </summary>
    /// <param name="valueList">The values you want the average of.</param>
    class method Average(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the trimmed average of a number of values
    /// </summary>
    /// <param name="valueList">The values you want the average of.</param>
    /// <param name="aTruncationPercentage">The percentage of items you want to truncate/trim. e.g. 0.5 means 50% -> 25% of the bottom values and 25% of the top values</param>
    class method TrimmedAverage(valueList: IEnumerable<Double>; aTruncationPercentage: Double): Double;
    /// <summary>
    /// Calculates the median of a number of values
    /// </summary>
    /// <param name="valueList">The values you want the median of.</param>
    /// <returns>The Median value</returns>
    class method Median(valueList: sequence of Double): Double;
    /// <summary>
    /// Calculates the percentage of a number compared to a total
    /// </summary>
    /// <param name="aNumber"></param>
    /// <param name="aTotal"></param>
    /// <returns></returns>
    class method CalculatePercentage(aNumber: Integer; aTotal: Integer): Double;
    /// <summary>
    /// Calculates the average plus 2 times the Standard Deviation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Plus2StandardDeviation_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average minus 2 times the Standard Deviation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Minus2StandardDeviation_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average plus 3 times the Standard Deviation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Plus3StandardDeviation_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average minus 3 times the Standard Deviation on the whole population
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Minus3StandardDeviation_P(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average plus 2 times the Standard Deviation on the sample population 
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Plus2StandardDeviation_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average minus 2 times the Standard Deviation on the sample population 
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Minus2StandardDeviation_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average plus 3 times the Standard Deviation on the sample population 
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Plus3StandardDeviation_S(valueList: IEnumerable<Double>): Double;
    /// <summary>
    /// Calculates the average minus 3 times the Standard Deviation on the sample population 
    /// </summary>
    /// <param name="valueList"></param>
    /// <returns></returns>
    class method Minus3StandardDeviation_S(valueList: IEnumerable<Double>): Double;
  end;
  
implementation

// Welford's method
// http://www.johndcook.com/standard_deviation.html
// http://stackoverflow.com/questions/895929/how-do-i-determine-the-standard-deviation-stddev-of-a-set-of-values
// Whole population
class method Statistical.StandardDeviation_P(valueList: IEnumerable<Double>): Double;
begin
  var M: Double := 0;
  var S: Double := 0;
  var k: Int32 := 1;
  for each value in valueList do 
  begin
    var tmpM := M;
    M := M + (value - tmpM) / k;
    S := S + (value - tmpM) * (value - M);
    inc(k)
  end;
  exit Math.Sqrt(S / (k - 1))
end;

// Welford's method
// http://www.johndcook.com/standard_deviation.html
// http://stackoverflow.com/questions/895929/how-do-i-determine-the-standard-deviation-stddev-of-a-set-of-values
// Sample population (default StdDev function in Excel, pre excel 2010)
class method Statistical.StandardDeviation_S(valueList: IEnumerable<Double>): Double;
begin
  var M: Double := 0;
  var S: Double := 0;
  var k: Int32 := 1;
  for each value in valueList do 
  begin
    var tmpM := M;
    M := M + (value - tmpM) / k;
    S := S + (value - tmpM) * (value - M);
    inc(k)
  end;
  exit Math.Sqrt(S / (k - 2))
end;

class method Statistical.CV_S(valueList: IEnumerable<Double>): Double;
begin
  result := StandardDeviation_S(valueList) / Average(valueList);
end;

class method Statistical.CV_P(valueList: IEnumerable<Double>): Double;
begin
  result := StandardDeviation_P(valueList) / Average(valueList);
end;

class method Statistical.Average(valueList: IEnumerable<Double>): Double;
begin
  result := 0;
  var len := 0;
  for each val in valueList do 
  begin
    result := result + val;
    inc(len);
  end;
  result := result / len;
end;

class method Statistical.CVPercentage_S(valueList: IEnumerable<Double>): Double;
begin
  result := CV_S(valueList) * 100;
end;

class method Statistical.CVPercentage_P(valueList: IEnumerable<Double>): Double;
begin
  result := CV_P(valueList) * 100;
end;

class method Statistical.CalculatePercentage(aNumber: Integer; aTotal: Integer): Double;
begin
  result := (Convert.ToDouble(aNumber) / Convert.ToDouble(aTotal)) * 100.0;
end;

class method Statistical.Plus2StandardDeviation_P(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_P(valueList);
  var avg := Statistical.Average(valueList);
  result := avg + (2 * stdDev);
end;

class method Statistical.Minus2StandardDeviation_P(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_P(valueList);
  var avg := Statistical.Average(valueList);
  result := avg - (2 * stdDev);
end;

class method Statistical.Plus3StandardDeviation_P(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_P(valueList);
  var avg := Statistical.Average(valueList);
  result := avg + (3 * stdDev);
end;

class method Statistical.Minus3StandardDeviation_P(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_P(valueList);
  var avg := Statistical.Average(valueList);
  result := avg - (3 * stdDev);
end;

class method Statistical.Plus2StandardDeviation_S(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_S(valueList);
  var avg := Statistical.Average(valueList);
  result := avg + (2 * stdDev);
end;

class method Statistical.Minus2StandardDeviation_S(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_S(valueList);
  var avg := Statistical.Average(valueList);
  result := avg - (2 * stdDev);
end;

class method Statistical.Plus3StandardDeviation_S(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_S(valueList);
  var avg := Statistical.Average(valueList);
  result := avg + (3 * stdDev);
end;

class method Statistical.Minus3StandardDeviation_S(valueList: IEnumerable<Double>): Double;
begin
  var stdDev := Statistical.StandardDeviation_S(valueList);
  var avg := Statistical.Average(valueList);
  result := avg - (3 * stdDev);
end;

//see https://en.wikipedia.org/wiki/Truncated_mean
method Statistical.TrimmedAverage(valueList: sequence of Double; aTruncationPercentage: Double): Double;
begin
  var sortedList := valueList.OrderBy(i -> i);
  var numberOfItems := valueList.Count();
  var numberOfItemsToTruncate := Integer(numberOfItems * (aTruncationPercentage / 2.0));
  result := 0;
  var len := 0;
  for each val in sortedList index i do 
  begin
    if (i > numberOfItemsToTruncate) and (i < (numberOfItems - numberOfItemsToTruncate)) then
    begin
      result := result + val;
      inc(len);
    end;
  end;
  result := result / len;
end;

method Statistical.Median(valueList: sequence of Double): Double;
begin
  var sortedList := valueList.OrderBy(i -> i);
  var listCount := sortedList.Count;
  var middlePos := listCount div 2;
  if middlePos >= listCount then exit Double.NaN;
  if middlePos < 0 then exit Double.NaN;
  exit sortedList.ElementAt(middlePos);
end;

end.
